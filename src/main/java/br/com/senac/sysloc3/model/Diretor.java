
package br.com.senac.sysloc3.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Diretor implements Serializable{
    
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    
    
    @Column(name = "id")
    private int id;
    
    
    @Column (name = "Primeiro_nome", nullable = false, length = 200)
    private String nome;
    
    @Column(name = "Ultimo_nome", nullable = false, length = 200)
    private String sobreNome;
    
    @Column(name = "local_nascimento", nullable = false, length = 200)
    private Date localNascimento;
    
    
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrimeiro_nome() {
        return nome;
    }

    public void setPrimeiro_nome(String primeiro_nome) {
        this.nome = primeiro_nome;
    }

    public String getSegundo_nome() {
        return sobreNome;
    }

    public void setSegundo_nome(String segundo_nome) {
        this.sobreNome = segundo_nome;
    }

    public Date getAno_nascimento() {
        return localNascimento;
    }

    public void setAno_nascimento(Date ano_nascimento) {
        this.localNascimento = ano_nascimento;
    }

    
    
    
    
}
