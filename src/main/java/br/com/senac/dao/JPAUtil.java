
package br.com.senac.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class JPAUtil {
    
    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("SyslocPU");
    
    public static EntityManager getEntityManager(){
        try {
            return emf.createEntityManager();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static void main(String[] args) {
        
    
        EntityManager e = JPAUtil.getEntityManager();
        if (e != null) {
            System.out.println("Conectou...");
        } else {
            System.out.println("Nao conectou ");
        }
    }
    
    
    
    
    
    
    
    
    
    
}
