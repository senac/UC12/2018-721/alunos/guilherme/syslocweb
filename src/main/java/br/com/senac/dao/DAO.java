
package br.com.senac.dao;

import javax.persistence.EntityManager;



public abstract class DAO<T> {
    
    protected EntityManager em;
    
   private final Class<T> entidade;
   
   public DAO(Class<T> Entidade) {
       this.entidade = Entidade;
   }
    
    public void save(T objeto) {
        
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.persist(objeto);
        em.getTransaction().commit();
        em.close();
    }
    
  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
