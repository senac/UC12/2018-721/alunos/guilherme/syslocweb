
package br.com.senac.sysloc3.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Ator {
    
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    
    private int id;
    
    private String primeiro_nome;
    private String segundo_nome;
    private String local_nascimento;
    private Date ano_nascimento;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrimeiro_nome() {
        return primeiro_nome;
    }

    public void setPrimeiro_nome(String primeiro_nome) {
        this.primeiro_nome = primeiro_nome;
    }

    public String getSegundo_nome() {
        return segundo_nome;
    }

    public void setSegundo_nome(String segundo_nome) {
        this.segundo_nome = segundo_nome;
    }

    public String getLocal_nascimento() {
        return local_nascimento;
    }

    public void setLocal_nascimento(String local_nascimento) {
        this.local_nascimento = local_nascimento;
    }

    public Date getAno_nascimento() {
        return ano_nascimento;
    }

    public void setAno_nascimento(Date ano_nascimento) {
        this.ano_nascimento = ano_nascimento;
    }
    
}
